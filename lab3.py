import random

import numpy as np


class Game:
    def __init__(self, a, b, c, d, e):
        # Проверка факторов выпукло-вогнутости
        Hxx = 2 * a < 0
        Hyy = 2 * b > 0

        if not (Hxx and Hyy):
            raise Exception('Игра не выпукло-вогнутая')

        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.e = e

    def get_variables(self):
        return self.a, self.b, self.c, self.d, self.e

    def H(self, x, y):
        a, b, c, d, e = self.get_variables()
        return a * x * x + b * y * y + c * x * y + d * x + e * y

    def solve_analytic(self):
        a, b, c, d, e = self.get_variables()

        # Производные функции ядра
        # Hx = 2*a*x + c*y + d
        # Hy = 2*b*y + c*x + e
        # x = -(c*y + d) / (2*a)
        # y = -(c*x + e) / (2*b)

        # Аналитическое решение
        # x = -(c*(-(c*x + e) / (2*b)) + d) / (2*a)
        # x = (-c*(-c*x-e)/(2*b) - d) / (2*a)
        # x = ((c*c*x + c*e)/(2*b) - d) / (2*a)
        # x = (c*c*x + c*e) / (4*a*b) - d / (2*a)
        # x - c*c*x/(4*a*b) = c*e  / (4*a*b) - d / (2*a)
        # x*(4*a*b - c*c)/(4*a*b) = c*e  / (4*a*b) - d / (2*a)
        # x*(4*a*b - c*c) = c*e - 2*b*d
        x = (c * e - 2 * b * d) / (4 * a * b - c * c)
        y = -(c * x + e) / (2 * b)

        if x < 0 or y < 0:
            raise Exception('x и y должны быть неотрицательными')

        H = self.H(x, y)
        return x, y, H

    def solve_grid(self, N):
        H_N = np.fromfunction(lambda x, y: self.H(x/N, y/N), (N + 1, N + 1))  # type: np.ndarray

        # Поиск седловой точки
        Hmax = np.amax(H_N, axis=0)
        Hmin = np.amin(H_N, axis=1)

        intersection = np.intersect1d(Hmax, Hmin)  # type: np.ndarray
        # Если седловой точки нет
        if len(intersection) == 0:
            X, Y = self.solve_brownrobinson(H_N)
            Xmax = np.max(X)
            Ymax = np.max(Y)

            i = np.where(X == Xmax)[0].item()
            j = np.where(Y == Ymax)[0].item()

            x = i / N
            y = j / N

            return False, x, y, H_N[i][j], H_N

        # Седловая точка существует
        # Вычисление ее координат
        x = np.where(Hmin == intersection.item())[0].item()/N
        y = np.where(Hmax == intersection.item())[0].item()/N

        return True, x, y, intersection.item(), H_N

    @staticmethod
    def indices(l, x):
        i = 0
        result = []
        while True:
            try:
                i = l.index(x, i)
            except ValueError:
                return result
            result.append(i)
            i += 1

    @staticmethod
    def solve_brownrobinson(C):

        xi = random.randint(0, C.shape[0] - 1)
        yi = random.randint(0, C.shape[0] - 1)

        X = np.zeros_like(C[0])
        Y = np.zeros_like(C[0])
        min_kvx = 0xFFFF_FFFF
        max_kvy = -0xFFFF_FFFF

        # Раз выводится 3 знака после запятой,
        # то точность выставлена в 4 знака
        signs = 4
        e_sign = 10 ** -signs

        cnt_X = np.zeros_like(C[0])
        cnt_Y = np.zeros_like(C[0])

        e = 1.0
        i = 0
        while e > e_sign:
            X += C.transpose()[yi]
            max_x = max(X)
            x_inds = Game.indices(list(X.tolist()), max_x)
            xin = random.choice(x_inds)

            kvx_i = max_x / (i + 1)
            min_kvx = min(min_kvx, kvx_i)
            cnt_X[xi] += 1

            Y += C[xi]
            min_y = min(Y)
            y_inds = Game.indices(list(Y.tolist()), min_y)
            yin = random.choice(y_inds)
            kvy_i = min_y / (i + 1)
            max_kvy = max(max_kvy, kvy_i)
            cnt_Y[yi] += 1

            e = min_kvx - max_kvy

            xi = xin
            yi = yin
            i += 1

        return cnt_X / i, cnt_Y / i




if __name__ == '__main__':
    np.set_printoptions(precision=3, suppress=True)
    # Пример выполнения работы
    # game = Game(-3, 3/2, 18/5, -18/50, -72/25)
    # Вариант 9
    game = Game(-6, 32/5, 16, -16/5, -64/5)
    ax, ay, aH = game.solve_analytic()

    print('Аналитическое решение:')
    print(f'x = {ax:.3f}; y = {ay:.3f}; H = {aH:.3f}')
    print()

    e = 0.0001
    N = 2
    H_prev = 2
    H = 1
    # for N in range(2, 11):
    while abs(H_prev - H) > e:
        H_prev = H
        res, x, y, H, H_N = game.solve_grid(N)

        print(f'N = {N}')
        print(H_N)

        if res:
            print('Есть седловая точка:')
        else:
            print('Седловой точки нет, решение методом Брауна-Робинсон:')

        print(f'x = {x:.3f}; y = {y:.3f}; H = {H:.3f}')
        print()

        N += 1

    # Вычисления погрешностей
    N_max = N
    for N in range(2, N_max):
        res, x, y, H, H_N = game.solve_grid(N)
        delta_x = abs(ax - x) / ax
        delta_y = abs(ay - y) / ay
        delta_H = abs(aH - H) / abs(aH)
        print(f"N = {N}")
        print(f"dx = {delta_x:.3f}")
        print(f"dy = {delta_y:.3f}")
        print(f"dH = {delta_H:.3f}")
        print()
